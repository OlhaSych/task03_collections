package com.epam;

import java.util.*;

public class MyPriorityQueue<T extends Comparable> {
    private List<T> list;
    private int size;

    public MyPriorityQueue() {
        list = new ArrayList<>();
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object o) {
        if (isEmpty())
            return false;
        for (int i = 0; i < size; i++) {
            if (list.get(i) == o)
                return true;
        }
        return false;
    }

    public boolean add(T t) {
        if (list.add(t)) {
            Collections.sort(list);
            return true;
        }
        return false;
    }

    public boolean remove(T t) {
        if (list.remove(t)) {
            Collections.sort(list);
            return true;
        }
        return false;
    }

    public T peek() {
        if (isEmpty())
            throw new NoSuchElementException("The priority queue is empty");
        return list.get(0);
    }
}
