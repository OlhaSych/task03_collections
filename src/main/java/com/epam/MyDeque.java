package com.epam;

import java.util.NoSuchElementException;

public class MyDeque<T> {
    private int first;
    private int last;
    private int size;
    private int capacity;
    private Object[] array;

    public MyDeque() {
        capacity = 1;
        array = new Object[capacity];
        first = 0;
        last = 0;
        size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void addFirst(T item) {
        if (item == null) throw new NullPointerException();
        if (isEmpty()) {
            array[first] = item;
            last++;
            size++;
        } else {
            while (first <= 0)
                enlargeArray();
            if (!isEmpty()) first--;
            array[first] = item;
            size++;
        }
    }

    public void addLast(T item) {
        if (item == null) throw new NullPointerException();
        if (last >= capacity) {
            enlargeArray();
            array[last] = item;
            last++;
            size++;
        }
    }

    public T removeFirst() {
        if (isEmpty()) throw new NoSuchElementException();
        size--;
        T t = (T) array[first];
        array[first] = null;
        first++;
        if (first == last) {
            first = 0;
            last = 0;
        }
        return t;
    }

    public T removeLast() {
        if (isEmpty()) throw new NoSuchElementException();
        size--;
        last--;
        T t = (T) array[last];
        array[last] = null;
        return t;
    }

    public T peekFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return (T) array[first];
    }

    public T peekLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return (T) array[last];
    }

    private void enlargeArray() {
        int newCapacity = capacity * 2;
        Object[] newArray = new Object[newCapacity];
        int newFirst = (newCapacity - size()) / 2;
        for (int i = first; i < last; ++i)
            newArray[newFirst++] = array[i];
        first = (newCapacity - size()) / 2;
        last = newFirst;
        array = newArray;
        capacity = newCapacity;
    }
}